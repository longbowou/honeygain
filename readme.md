# Honeygain

With Honeygain, you can make money by simply sharing your Internet.

## How to run it?

- Clone the repository
- Run

```bash
docker-compose up -d
```

- To run multi honeygain

```bash
docker-compose up -d --scale honeygain=2
```
